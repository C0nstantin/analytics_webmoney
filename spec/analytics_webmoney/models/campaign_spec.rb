require 'spec_helper'

describe AnalyticsWebmoney::Campaign do
  before(:all) do
    AnalyticsWebmoney::Project.destroy_all
    s = AnalyticsWebmoney::Project.create({name: 'test_project fo campaigns'})
    AnalyticsWebmoney::Campaign.create({name: 'test campaigns',project_id: s.id})
  end

  let!(:project) {AnalyticsWebmoney::Project.all.first}

  it 'get all campaign for project' do
    expect(project.campaigns.count).to be > 0
    expect(project.campaigns.count).to eq subject.class.all(project.id).count
  end

  it 'find campaign by id' do
    expect(subject.class.find(project.campaigns.first.id)).to be_kind_of(AnalyticsWebmoney::Campaign)
  end

  it 'create new campaing with method craete' do
    expect(subject.class.create({name: 'res', project_id: project.id})).to be_kind_of(AnalyticsWebmoney::Campaign)

  end

  it 'create with method new and save' do
    expect {
    s = AnalyticsWebmoney::Campaign.new(name: '4444', project_id: project.id)
    s.save
    #expect(subject.class.create)
    }.to change{project.relations_reload.campaigns.count}.by(1)
  end

  it 'update with save' do
    s = AnalyticsWebmoney::Campaign.create(name:'www', project_id:project.id)
    s.name = '333'
    s.reload
    expect(s.name).to eq '333'
  end

  it 'destroy' do
    expect {project.campaigns.first.destroy}.to change{
      project.relations_reload.campaigns.count
    }.by(-1)

  end

  it 'destroy_all for project' do
    expect{ project.campaigns.destroy_all }.to change{ project.campaigns.count }.to(0)
  end


end
