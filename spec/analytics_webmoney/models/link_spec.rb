require 'spec_helper'

describe AnalyticsWebmoney::Link do
  before(:all) do
    AnalyticsWebmoney::Project.destroy_all
    pr = AnalyticsWebmoney::Project.create({name: 'for test link'})
    cm = AnalyticsWebmoney::Campaign.create({name: 'for test campaign',project_id: pr.id})
    AnalyticsWebmoney::Link.create({channel: '1', origin_url:'http://webmoney.ru', refer_url: 'http://yandex.ru', campaign_id: cm.id})

  end

  let!(:project) { AnalyticsWebmoney::Project.all.first }
  let!(:campaign) { project.campaigns.first }
  let!(:link) { campaign.links.first }

  it 'link present' do
    expect(link).to be_kind_of(AnalyticsWebmoney::Link)
    expect(link.id).not_to be_empty
  end

  it 'links.all' do
    expect(project.campaigns.first.links.count).to be > 0
  end

  it 'find by id' do
    expect(subject.class.find(link.id)).to be_kind_of(AnalyticsWebmoney::Link)
  end

  it 'create new link with save' do
    expect {
      s=AnalyticsWebmoney::Link.new(channel:'1', origin_url: 'https://web.money', refer_url: 'http://wmtransfer.com', campaign_id: campaign.id )
      s.save
    }.to change {campaign.relations_reload.links.count}.by(1)
  end

  it 'update with save' do
     expect{
      link.channel='11111'
      p link.inspect
      link.save
      link.reload
    }.to change{
      AnalyticsWebmoney::Link.find(link.id).channel
    }.from('1').to('11111')
  end

  it 'change only permitted' do
    expect {
     link.created_at=Time.now()
     link.save
    }.to_not change{ link.reload.created_at }
  end

  it 'destroy' do
    expect { link.destroy }.to change {campaign.relations_reload.links.count }.by(-1)
  end
end
