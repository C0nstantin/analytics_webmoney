require 'spec_helper'

# + Project.create()
#Project.find(id).destroy
#Project.find(id).update(params)
#s = Project.find(id); s.name = 'newtestvalue'; s.save
#s =Project.new(params); s.save
#s.campaigns. -> Campagins.all(project_id=id)
#Camaigns.create(params) params[project_id] must be set
#Campaigs.new(params) before save params[project_id] must be set
#s = Camapaigns.find(id) or s = Project.find(project_id).campaigns.first
#s.destroy
#s.update(params)
#s.name ='new name' s.save;
#s.project

describe AnalyticsWebmoney::Project do
  before(:all) do
    AnalyticsWebmoney::Project.destroy_all if  AnalyticsWebmoney::Project.all.count > 0
    AnalyticsWebmoney::Project.create({name:'test2'})
  end
  let! (:project) {subject.class.all.first}

  it 'all' do
    expect(subject.class.all).to be_kind_of(Array)
  end

  it 'all.first' do
    expect(subject.class.all.first).to be_kind_of(AnalyticsWebmoney::Project)
    expect(subject.class.all.first.name).to eq('test2')
  end



  it 'find' do
    expect(subject.class.find(project.id)).to be_kind_of(subject.class)
  end

  it 'save' do
    pending("Project not found in external server") unless project.id.present?
    project.name='test222'
    project.save
    expect(subject.class.find(project.id).name).to eq('test222')
    project.name='test333'
    project.save
    expect(subject.class.find(project.id).name).to eq('test333')
  end

  it 'relation' do
    expect(project.campaigns).to be_kind_of(Array)
  end
  it 'relation create' do
    pending("Feature for next version")
    expect{project.campaigns << Camapaigns.new({name: 'test'})}.to change{Campaing.all(project.id).from(0).to(1)}
  end

  it 'not found raise' do
    expect {subject.class.find('111111')}.to raise_error(/404/)
  end


  it 'project create' do
    s = subject.class.create({name: '111'});
    expect(s).to be_kind_of(subject.class)
    expect(s.id).not_to be_empty
    expect(s.name).to eq('111')
    expect(s.persisted?).to be true
  end

  it 'project create without nama' do
    s = subject.class.new
    expect {s.save}.to raise_error(/400/)
  end


end
