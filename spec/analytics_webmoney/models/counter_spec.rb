require 'spec_helper'

describe AnalyticsWebmoney::Counter do
  before(:all) do
    AnalyticsWebmoney::Project.destroy_all
    pr = AnalyticsWebmoney::Project.create({name: 'for test counters'})
    cm = AnalyticsWebmoney::Campaign.create({name: 'for test campaign',project_id: pr.id})
    AnalyticsWebmoney::Counter.create({name: 'test1', campaign_id: cm.id})

  end

  let!(:project) { AnalyticsWebmoney::Project.all.first }
  let!(:campaign) { project.campaigns.first }
  let!(:counter) { campaign.counters.first }

  it 'counter present' do
    expect(counter).to be_kind_of(AnalyticsWebmoney::Counter)
    expect(counter.id).not_to be_empty
  end

  it 'counters.all' do
    expect(project.campaigns.first.counters.count).to be > 0

  end

  it 'find by id' do
    expect(subject.class.find(counter.id)).to be_kind_of(AnalyticsWebmoney::Counter)
  end

  it 'create new counter with save' do
    expect {
      s=AnalyticsWebmoney::Counter.new(name:'testss1', campaign_id: campaign.id )
      s.save
    }.to change {campaign.relations_reload.counters.count}.by(1)
  end

  it 'update with save' do
     expect{
      counter.name='11111'
      counter.save
      counter.reload
    }.to change{
      AnalyticsWebmoney::Counter.find(counter.id).name
    }.from('test1').to('11111')
  end

  it 'change only permitted' do
    expect {
     counter.created_at=Time.now()
     counter.save
    }.to_not change{ counter.reload.created_at }
  end

  it 'destroy' do
    expect { counter.destroy }.to change {campaign.relations_reload.counters.count }.by(-1)
  end
end
