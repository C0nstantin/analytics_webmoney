require 'spec_helper'

describe AnalyticsWebmoney::API::Project do
  context 'all' do
    it 'status 200'
    it 'type body json array'
    it 'have some projects with fields'
  end

  context 'find' do
    it 'status 200'
    it 'json with fields'
    it 'if id not preset raise error'
    it 'if id not find in server raise 404'
  end

  context 'create' do
    it 'create new project'
    it 'raise error if name not found'
  end

  context 'update' do
    it 'update projets'
    it 'raise error if data request not valid'
  end
end
