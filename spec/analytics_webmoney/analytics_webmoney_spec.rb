describe AnalyticsWebmoney do
  it "default config token" do
    expect(subject.configuration.token).not_to eq('')
  end
end

describe AnalyticsWebmoney::VERSION do
  it 'version' do
    expect(subject).to match(/\d\.\d\.\d/)
  end
end


