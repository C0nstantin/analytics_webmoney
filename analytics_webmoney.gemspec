# coding: utf-8
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "analytics_webmoney/version"

Gem::Specification.new do |spec|
  spec.name          = "analytics_webmoney"
  spec.version       = AnalyticsWebmoney::VERSION
  spec.authors       = ["Constantin Karataev"]
  spec.email         = ["karataev@wmid.com"]

  spec.summary       = %q{ ..}
  spec.description   = %q{.}
  spec.homepage      = "https://analytics.webmoney.ru"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
	  spec.metadata["allowed_push_host"] = "https://bonus.webmoney.ru"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
  spec.add_dependency 'rest-client', '~>2.0'

  spec.add_development_dependency "bundler", "~> 1.15"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.7"
  spec.add_development_dependency "activesupport", "~> 0"
  spec.add_development_dependency "activemodel", "~> 0"
end
