module AnalyticsWebmoney
  module Model
    extend ActiveSupport::Concern
    include ActiveModel::Model
    include ActiveModel::Validations
    include ActiveModel::Serializers
    include ActiveModel::Serialization
    attr_accessor :params, :children, :parent

    class_methods do
      class_variable_set(:@@parent_name, nil)

      def attr_permitted *args
        class_variable_set(:@@permit_params,args.to_a)
      end

      def all parent_id = nil
        res = Items.new()
        res.parent_id = parent_id
        res.current_class = self
        current_class_api.all(parent_id).body[self.cname.pluralize].each do |s|
          res << self.new(s)
        end
        res
      end

      def cname
        self.name.split('::').last.downcase.to_s
      end

      def current_class_api
        Object.const_get("AnalyticsWebmoney::API::#{self.cname.capitalize}")
      end

      def permitted_params params
        s = {}
        self.class_variable_get(:@@permit_params).each do |p|
          s["#{p}"] = params["#{p}"] || params[:"#{p}"]
        end
        s
      end

      def find(id)
        self.new (current_class_api.find(id).body[self.cname])
      end

      def create(params)
        item = self.new(self.permitted_params(params))
        raise Error::RecordNotValid unless item.valid?
        item.save
        if item.persisted?
          item
        else
          raise Error::RecordNotSaved
        end
      end

      def belongs_to name
        cur_class_name = "AnalyticsWebmoney::" + name.to_s.capitalize
        class_variable_set(:@@parent_name, name)
        define_method (name.to_sym) do
          if parent.nil?
            self.parent = Object.const_get(cur_class_name).find(self.send("#{name}_id"))
          end
          self.parent
        end
      end

      def has_many rel_name
        cur_class_name = "AnalyticsWebmoney::" + rel_name.to_s.capitalize.singularize
        define_method("#{rel_name}") do
          self.children = {} if self.children.nil?
          if self.children[rel_name].nil?
            self.children[rel_name] = Object.const_get(cur_class_name).send(:all, self.id)
          end
          self.children[rel_name]
        end
        #end
      end

      def destroy_all
        self.all.each do |s|
          s.destroy
        end
      end
    end


    included do

      def initialize params = {}
        self.params = params
        super self.params
      end

      def params_for_save
        s = {}
        self.class.class_variable_get(:@@permit_params).each do |p|
          s["#{p}"]=self.send(:"#{p}")
        end
        s
      end

      def params=(params)
        new_params = {}
        params.each  do |k,v|
          if self.respond_to?("#{k}=")
            self.send("#{k}=",v)
            new_params[k]=v
          end
        end
        super new_params
      end

      def save
        if @id.present?
          self.update
        else
          self.create
        end
      end

      def reload
        self.class.find(@id)
      end

      def relations_reload
        self.children = nil
        self.children = {}
        self.parent = nil
        self
      end

      def destroy
        self.class.current_class_api.delete(@id)
      end

      def update(params=nil)
        self.params = self.class.new(self.class.permitted_params(params)).params_for_save unless params.nil?
        self.params = self.class.current_class_api.update( @id, {"#{self.class.cname}": self.params_for_save} ).body[self.class.cname]
        self
      end

      def create
        self.params = self.class.current_class_api.create(self.class.permitted_params(self.params), self.parent_id).body[self.class.cname]
        self
      end

      def persisted?
        begin
          s = self.class.find(self.id)
          if s.id == self.id
            return true
          else
            return false
          end
        rescue RestClient::NotFound
          return false
        end
      end

      def parent_id
        if(self.class.class_variable_defined?(:@@parent_name))
          parent_name = self.class.class_variable_get(:@@parent_name)
        else
          parent_name =nil
        end
        self.params["#{parent_name}_id"]
      end
    end

    class Items < Array
      attr_accessor :current_class, :parent_id
      def destroy_all
        self.each do |i|
          i.destroy if i.respond_to?(:destroy)
        end
        self.clear
      end

      def add(params)
        s = Hash.new
        s["#{self.current_class.class_variable_get(:@@parent_name)}_id"]=self.parent_id
        self.current_class.create(params.merge(s))
      end

    end
  end

end
