module AnalyticsWebmoney
  module Model
    module Error
      class CustomError < StandardError
        attr_reader :description
        def initialize (description = nil)
          super(description)
          @description = description

        end
      end

      class RecordNotValid < CustomError;  end
      class RecordNotSaved < CustomError; end
      #class Forbidden < CustomError;                  self.code = 403 end
      #class NotFound < CustomError;                   self.code = 404 end
      #class TooManyRequests < CustomError;            self.code = 429 end
      #class Internal < CustomError;                   self.code = 500 end
      #class ExternalServiceError < CustomError;       self.code = 503 end
    end
  end
end
