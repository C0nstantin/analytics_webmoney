module AnalyticsWebmoney
  class Campaign
    include Model

    attr_accessor :id, :project_id, :name, :created_at, :updated_at, :links
    attr_permitted :project_id, :name

    belongs_to :project
    has_many :links
    has_many :counters

    validates! :project_id, presence: true
    validates! :name,       presence: true

    def statistics
      params = Hash.new
      params['request'] = {ids:  self.links.to_a}
      params['request']['metrics'] = "clicks"
      params['request']['date_range']= {
        start_date: self.start_date,
        end_date: self.end_date,
        interval: "month"
      }
      res = RestClient::Request.execute(
        url: AnalyticsWebmoney::configuration.api_url + "/statistics",
        method: :post,
        headers: AnalyticsWebmoney::API::Request.headers,
        verify_ssl: false,
        payload: params.to_json
      )
      return ActiveSupport::JSON.decode(res.body)

    end
  end
end
