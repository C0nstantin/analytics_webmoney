module AnalyticsWebmoney
  class Project
    include Model

    attr_accessor :name, :id, :created_at, :updated_at, :users, :campaigns
    attr_permitted :name

    has_many :campaigns

    validates! :name, presence: true
  end
end
