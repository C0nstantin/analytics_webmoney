module AnalyticsWebmoney
  class Link
    include Model

    attr_accessor :id, :campaign_id, :channel, :origin_url, :refer_url, :target_url, :created_at, :updated_at, :public_id
    attr_permitted :campaign_id, :channel, :origin_url, :refer_url, :public_id

    belongs_to :campaign

    validates! :campaign_id, presence: true
    validates! :channel,     presence: true
    validates! :origin_url,  presence: true
    validates! :refer_url,   presence: true
    #after_craete:  create analytics_link

#{
#  "request": {
#    "agents" [{shop_id: xxx, purse_id: xxx}],
#    "date_range": {
#      "start_date": "2018-01-01T00:00:00.000+00:00",
#      "end_date": "2018-01-01T00:00:00.000+00:00"
#    }
#  }
#}
#postfix may be "_all" for all payments
    def get_payments_all agents={}, start_date = DateTime.now()-1.day, end_date = DateTime.now()
      get_payments agents, start_date, end_date, postfix="_all"
    end

    def get_payments agents={}, start_date = DateTime.now()-1.day, end_date = DateTime.now(), postfix=""

      params = Hash.new()

      params['request'] = { agents: agents, "date_range": {"start_date": start_date, "end_date": end_date } }

      res = RestClient::Request.execute(
        url: AnalyticsWebmoney.configuration.api_url + "/links/#{self.id}/payments"+postfix,
        method: :post,
        headers: AnalyticsWebmoney::API::Request.headers,
        verify_ssl: false,
        payload: params.to_json
      )
      return ActiveSupport::JSON.decode(res.body)
    end

    def clicks( start_date=DateTime.now()-3.years, end_date=DateTime.now())
      params = Hash.new()
      params['request'] = {ids: [self.id],metrics: ['clicks'], "date_range": {"start_date": start_date, "end_date": end_date} }
      res = RestClient::Request.execute(
        url: AnalyticsWebmoney::configuration.api_url + "/statistics",
        method: :post,
        headers: AnalyticsWebmoney::API::Request.headers,
        verify_ssl: false,
        payload: params.to_json
      )
      return  ActiveSupport::JSON.decode(res.body)['reports']['clicks']
    end
  end

end
