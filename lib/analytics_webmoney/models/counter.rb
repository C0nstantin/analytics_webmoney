module AnalyticsWebmoney
  class Counter
    include Model
    belongs_to :campaign

    attr_accessor :id, :campaign_id,  :usage_type, :name, :created_at, :updated_at
    attr_permitted :campaign_id, :usage_type, :name

    validates! :campaign_id, presence: true
    #validates! :usage_type,  presence: true
    validates! :name,        presence: true
  end
end
