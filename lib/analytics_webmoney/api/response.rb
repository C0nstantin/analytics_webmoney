module AnalyticsWebmoney
  module API
    class Response
      attr_reader :source
      attr_reader :body

      def initialize(response)
        @source = response
        if @source.is_a?(RestClient::Response)
          if @source.code == 200
            @body = JSON.parse(@source.body)
          elsif @source.code == 204
            @body = ''
          else
            nil
            #raise Errors::ExternalServiceError.new("#{response.code} #{response.body}")
          end
        end
      end


    end
  end
end
