module AnalyticsWebmoney
  module API
    module ClassMethods

      @@name = ''
      @@parent = nil

      def request url, method = :get, params={}
        r = RestClient::Request.execute(
          url: AnalyticsWebmoney.configuration.api_url + url,
          method: method,
          headers: headers,
          verify_ssl: false,
          payload: params.to_json
        )
        AnalyticsWebmoney::API::Response.new(r)
      end


      def headers
        {:content_type => "application/json", "Authorization": "Token token=#{AnalyticsWebmoney.configuration.token}"}
      end

      def all parent_id = nil
        request url(parent_id)
      end

      def find id
        request url + "/#{id}"
      end

      def create params={}, parent_id = nil
        request url(parent_id), :post, params
      end

      def update id, params={}
        request url + "/#{id}" , :patch, params
      end

      def delete id
        request url + "/#{id}", :delete
      end

      def url id = nil
        return "/#{class_variable_get(:@@name).pluralize}" if id.nil? || class_variable_get(:@@parent).nil?
        "/#{class_variable_get(:@@parent).pluralize}/#{id}/#{class_variable_get(:@@name).pluralize}"
      end
    end

    class Request
      extend ClassMethods
    end

    parents = {
      project:  nil,
      campaign: :project,
      link:  :campaign,
      counter: :campaign,
      payment: :link
    }
    parents.each do |k,v|
      s = Class.new(Request)
      s.class_variable_set(:@@name,k.to_s)
      s.class_variable_set(:@@parent, v.nil? ? nil : v.to_s)
      self.const_set(k.to_s.capitalize,s)
    end

  end
end
