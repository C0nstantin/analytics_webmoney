require "analytics_webmoney/version"
require 'active_support'
require 'rest-client'
require 'active_model'
require 'analytics_webmoney/api/request'
require 'analytics_webmoney/api/response'
require 'analytics_webmoney/models/model'
require 'analytics_webmoney/models/project'
require 'analytics_webmoney/models/campaign'
require 'analytics_webmoney/models/link'
require 'analytics_webmoney/models/counter'
#equire 'analytics_webmoney/projects'
#require 'analytics_webmoney/models/project'
require 'analytics_webmoney/models/exception'
require 'json'
module AnalyticsWebmoney


  class << self
    attr_accessor :configuration
  end

  def self.configure
    self.configuration ||= Configuration.new
    yield(configuration)
  end

  class Configuration
    attr_accessor :api_url
    attr_accessor :token
    attr_accessor :service_wmid
    attr_accessor :project_id

    def initialize
      @api_url = 'https://analytics.webmoney.ru/api'
      @token = ''
      @service_wmid = ''
      @project_id = ''
    end
  end
end

